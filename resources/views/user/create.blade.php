@extends('templates.main')

@section('content')
	<div class="col-md-12">
		<h1 class="text-left">Usuário - <small>Cadastrar</small></h1>
		@if (isset($msg))
			@if ($success == true)
				@php $class = 'alert-success' @endphp
			@else
				@php $class = 'alert-danger' @endphp
			@endif
			<div class="alert {{ $class }}">
				{{ $msg }}
			</div>
		@endif

		{!! Form::open(['route' => 'user.store', 'class' => 'form']) !!}
			<div class="form-group{{ $errors->has('name') ? ' has-error':'' }}">
				{!! Form::label('name','Nome') !!}
				{!! Form::text('name', null, ['class' => 'form-control','placeholder' => 'Informe o seu nome']) !!}
				@if($errors->has('name'))
					<span class="help-block">
						{{ $errors->first('name') }}
					</span>
				@endif
			</div>

			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				{!! Form::label('email', 'E-mail') !!}
				{!! Form::input('text', 'email', null, ['class' => 'form-control', 'placeholder' => 'Informe seu e-mail']) !!}
			</div>
			
			<div class="form-group">
				{!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
			</div>
		{!! Form::close() !!}
	</div>
@endsection