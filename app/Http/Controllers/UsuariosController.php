<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UsuariosFormRequest;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "Aqui será a lista de usuários";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuariosFormRequest $request)
    {
        /*$validator = $this->validaNome($request);
        if ($validator->fails()){
            return redirect()->back()->withInput()->with(['errors'=> $validator->errors()]);
        }
        */
        echo "Controller";
        /*$dataForm = $request->all();
        $user = User::create($dataForm);
        if ($user) {
            return view('user.create')->with(['success' => true, 'msg' => 'Cadastro realizado com sucesso!']); //redirect()->route('user.create')->withInput()->with(['success' => true, 'msg' => 'Cadastro realizado com sucesso!']);
        }*/ 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function validaNome($request){
        //dd(count(collect(explode(' ', $request->name))));
        //$nome = explode(' ', $request->name);
        //dd(collect($nome));
        //$sizeName = count(collect($nome));
        //dd(count(collect($nome)));
        //$first = collect($nome)->first();
        //dd(sizeof(collect($nome)->first()));
        //$last = collect($nome)->last();
        //dd(strlen($last));
        /*$this->validate($request, [
            'name' => [
                'required',
                function ($attribute, $value, $fail)
                {
                    if (count(collect(explode($value))) == 1) {
                        return $fail('O campo {$attribute} é inválido');
                    }
                }
            ]
        ]);*/
        $validator = Validator::make($request->all(),[
            'name' => 'required|sizeName|firstName|lastName',
        ]);
            ///(?=^.{2,60}$)^[A-ZÀÁÂĖÈÉÊÌÍÒÓÔÕÙÚÛÇ][a-zàáâãèéêìíóôõùúç]+(?:[ ](?:das?|dos?|de|e|[A-Z][a-z]+))*$/

        //$validator->sometimes('name', 'required', function ($input) {
            //$nome = count(collect(explode(' ', $input->name)));
        //    return $input->name == 'luiz';
        //});
        return $validator;
    }
    
}
