<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('sizeName', function ($attribute, $value, $parameters, $validator) {
            $value = count(collect(explode(' ', $value)));
            return $value != 1;
        });
        
        Validator::replacer('sizeName', function($message, $attribute, $rule, $parameters) {
            return str_replace($message, "Nome inválido", $message);
        });

        Validator::extend('firstName', function ($attribute, $value, $parameters, $validator) {
            $nome = explode(' ', $value);
            $value = collect($nome)->first();
            return (strlen($value) > 2) || ($value == 'D') || ($value == 'I') || ($value == 'O') || ($value == 'U') || ($value ==  'Y') || ($value == 'Í') || ($value == 'Ó') || ($value == 'Ô') || ($value == 'Õ') || ($value == 'Ú');
        });
        
        Validator::replacer('firstName', function($message, $attribute, $rule, $parameters) {
            return str_replace($message, "Primeiro nome inválido", $message);
        });

        Validator::extend('lastName', function ($attribute, $value, $parameters, $validator) {
            $nome = explode(' ', $value);
            $value = collect($nome)->last();
            return (strlen($value) > 1) || ($value == 'I') || ($value == 'O') || ($value == 'U') || ($value ==  'Y') || ($value == 'Í') || ($value == 'Ó') || ($value == 'Ô') || ($value == 'Õ') || ($value == 'Ú');
        });

        Validator::replacer('lastName', function($message, $attribute, $rule, $parameters) {
            return str_replace($message, "Último nome inválido", $message);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
