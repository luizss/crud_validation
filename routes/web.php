<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('usuarios')->group(function(){
    Route::get('/list','UsuariosController@index')->name('usuarios.index');
    Route::get('/novo','UsuariosController@create')->name('user.create');
    Route::post('/salvar','UsuariosController@store')->name('user.store');
    Route::get('/{user}/editar','UsuariosController@edit')->name('user.edit');
    Route::patch('/update/{user}','UsuariosController@update')->name('user.update');
    Route::delete('/delete/{user}','UsuariosController@delete')->name('user.delete');
});

